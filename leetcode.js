const axios = require('axios');
const objectsToCsv = require('objects-to-csv');
const fs = require('fs');

function getApiURL() {
	// Returns a String denoting the API url which fetches all problems

}

async function getAllProblems() {
	/* Returns a Promise object of the response on calling
		the API to fetch all problems
	*/

}

function filterPaidProblems(data) {
	/* Returns an Array with data on problems that aren't for paid users only
	Input:
		data - raw JSON data from the API response
	Output:
		Array of objects each denoting a free problem
	*/

}

function sortBySubmission(data) {
	/* Returns data sorted by total submissions count in descending order
	Input:
		data - Array of objects denoting the problems with the id, question_title, submissions properties
	Output:
		input data sorted by the "submissions" property in descending order	
	*/

}

function getTopNProblems(N, data) {
	/* Returns top n problems
	Input:
		N - number of problems to return
		data - Array of objects denoting the problems with the id, question_title, submissions properties
	Output:
		Slice of top N problems 	
	*/

}

function reducer(accumulator, object) {
	/* Returns an Array of objects reduced in its number of properties
	Input:
	 	accumulator - object which accumulates the reduced data
        object - Object with properties question_id, question__article__live, question__article__slug, question__title, question__title_slug, 
        question__hide, total_acs, total_submitted, frontend_question_id, is_new_question
	Output:
	 	accumulator storing data reduced to the properties: id denoting question_id, question__title, and submissions denoting total_submitted
	*/

}

function transformData(data) {
	/*
	Returns transformed data with only required properties
	Input:
        data - Array of objects with properties question_id, question__article__live, question__article__slug, question__title, question__title_slug,
        question__hide, total_acs, total_submitted, frontend_question_id, is_new_question
	Output:
	 	Array of objects with properties id denoting question_id, question__title, and submissions denoting total_submitted

	*/	
	let transformedData = data.reduce(reducer,[]);

    return transformedData;

}

function getTopHundredProblems(allProblems) {
	/* Returns the top 100 most submitted problems

	Input:
		allProblems - Raw API response
	Output:
		Array of objects with the question id, title and total submissions values for the 
	 top 100 problems ordered by their total submissions in descending order
	*/

}

async function writeToCSV(data, destination) {
	/* Write data to a CSV file
	Input	
		data - data to write
		destination - CSV file destination

	*/	

}

async function main() {
	// Fetches data from the API and stores 
	// question id, title and total submissions for top 100 problems to CSV file
    let allProblems = await getAllProblems();
    console.log(allProblems);
	// fs.writeFile("./problems-all.json", JSON.stringify(allProblems, null, 4), (err) => {
	// 	if (err) {
	// 	    console.error(err);
	// 	    return;
	// 	};
	// });


    // TODO: Uncomment after implementing getTopHundredProblems()
    //  let topHundredProblems = await getTopHundredProblems(allProblems);

    
    // TODO: Uncomment after implementing writeToCSV()
    //  writeToCSV(topHundredProblems, './list.csv');

}

module.exports = {getApiURL, getAllProblems, getTopHundredProblems, writeToCSV};