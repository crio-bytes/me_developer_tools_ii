const functions = require('../leetcode');

const axios = require('axios');

const objectsToCsv = require('objects-to-csv');

const fs = require('fs');



jest.mock('axios');



describe('API URL:', () => {

    it('It must be correct', () => {

        expect(functions.getApiURL()).toEqual('https://leetcode.com/api/problems/all/');

    });

});


/*
Test for the getAllProblems() method. Uncomment after implementing the method
*/

// describe('Tests for API interaction', () => {

//     it('', async () => {

//         var problemData = require('../data.json');

//         axios.get.mockResolvedValue({ data: problemData });

//         let allproblems = await functions.getAllProblems();

//         expect(axios.get).toHaveBeenCalled();

//         expect(axios.get).toHaveBeenCalledWith('https://leetcode.com/api/problems/all/');

//         expect(allproblems).toEqual(problemData);

//     });

// });


/*
Test for the getTopHundredProblems() method. Uncomment after implementing the method
*/

// describe('Tests for getting top 100 problems', () => {

//     it('', async () => {

//         var problemData = require('../data.json');

//         var topHundredProblemsMocked = getTopHundredProblemsMocked(problemData);

//         var topHundredProblemsReal = functions.getTopHundredProblems(problemData);

//         expect(topHundredProblemsMocked).toEqual(topHundredProblemsReal);

//     });

// });



/*
Test for the writeToCSV() method. Uncomment after implementing the method
*/

// describe('Tests for CSV file', () => {

//     it('', async () => {

//         var problemData = require('../data.json');

//         var topHundredProblemsMocked = getTopHundredProblemsMocked(problemData);

//         await writeToCSVMocked(topHundredProblemsMocked);

//         await functions.writeToCSV(topHundredProblemsMocked, './list.csv');

//         var dataReal = fs.readFileSync('./list.csv');

//         var dataMocked = fs.readFileSync('./listMocked.csv');

//         expect(dataReal.toString()).toEqual(dataMocked.toString());

//     });

// });



function filterPaidProblems(data) {

    let dataToFilter = data.stat_status_pairs;

    let filteredData = dataToFilter.filter(problem => !(problem.paid_only))

    return filteredData;

}



function compare(first, second) {

    return second.submissions - first.submissions;

}



function sortBySubmission(data) {

    return data.sort(compare);

}



function getTopNProblems(N, data) {

    return data.slice(0, N);

}



function reducer(accumulator, object) {

    accumulator.push({

        id: object.stat.frontend_question_id,

        question_title: object.stat.question__title,

        submissions: object.stat.total_submitted

    });

    return accumulator;

}



function transformData(data) {

    let transformedData = data.reduce(reducer, []);

    return transformedData;

}



function getTopHundredProblemsMocked(allProblems) {

    let unpaidProblems = filterPaidProblems(allProblems);

    let transformedProblems = transformData(unpaidProblems);

    let problemsSortedBySubmission = sortBySubmission(transformedProblems);

    let topHundredProblems = getTopNProblems(100, problemsSortedBySubmission);

    return topHundredProblems;

}



async function writeToCSVMocked(topHundredProblems) {

    const csv = new objectsToCsv(topHundredProblems);

    await csv.toDisk('./listMocked.csv')

}